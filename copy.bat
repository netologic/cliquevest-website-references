@ECHO OFF
COLOR 07

ECHO ************* Copy jQuery
XCOPY src\jquery\dist\jquery.js                                                     distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

ECHO ************* Copy Bootstrap
XCOPY src\bootstrap\dist\js\bootstrap.js                                            distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR
XCOPY src\bootstrap\dist\css\bootstrap.css                                          distr\content\ /Y
IF errorlevel 1 GOTO ERROR
XCOPY src\bootstrap\fonts\glyphicons-halflings-regular.*                            distr\content\fonts\ /Y
IF errorlevel 1 GOTO ERROR
XCOPY src\bootstrap\less\*.less                                                     distr\content\less\system\bootstrap\ /Y
IF errorlevel 1 GOTO ERROR

ECHO ************* Copy backbone
XCOPY src\backbone\backbone.js                                                      distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy html5shiv
REM XCOPY src\html5shiv\dist\html5shiv.js                                               distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR

ECHO ************* Copy underscore
XCOPY src\underscore\underscore.js                                                  distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy underscore string
REM XCOPY src\underscore.string\lib\underscore.string.js                                distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy Font Awesome
REM XCOPY src\Font-Awesome\less\*.less                                                  distr\content\less\system\fontawesome\ /Y
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\Font-Awesome\css\font-awesome.css                                         distr\content\ /Y
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\Font-Awesome\fonts\fontawesome-webfont.*                                  distr\content\fonts\ /Y
REM IF errorlevel 1 GOTO ERROR

ECHO ************* Copy jQuery validation
XCOPY src\jquery-validation\dist\jquery.validate.js                                 distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

ECHO ************* Copy Respond
XCOPY src\respond\src\respond.js                                                    distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy Cleditor
REM XCOPY src\cleditor\jquery.cleditor.js                                               distr\scripts\plugins\ /Y
REM IF errorlevel 1 GOTO ERROR
REM DEL distr\scripts\plugins\plugin.cleditor.js /Q
REM IF errorlevel 1 GOTO ERROR
REM REN distr\scripts\plugins\jquery.cleditor.js                                        plugin.cleditor.js
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\cleditor\jquery.cleditor.less                                             distr\content\less\plugins\ /Y
REM IF errorlevel 1 GOTO ERROR
REM DEL distr\content\less\plugins\plugin.cleditor.less /Q
REM IF errorlevel 1 GOTO ERROR
REM REN distr\content\less\plugins\jquery.cleditor.less                                 plugin.cleditor.less
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\cleditor\images\*.gif                                                     distr\content\images\cleditor\ /Y
REM IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy jQuery browser
REM XCOPY src\jquery-browser-plugin\dist\jquery.browser.js                              distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy jQuery ui
REM XCOPY src\jquery-ui\ui\jquery.ui.core.js                                            distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\jquery-ui\ui\jquery.ui.widget.js                                          distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\jquery-ui\ui\jquery.ui.mouse.js                                           distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\jquery-ui\ui\jquery.ui.draggable.js                                       distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR
REM XCOPY src\jquery-ui\ui\jquery.ui.droppable.js                                       distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR

REM ECHO ************* Copy jQuery scrollTo
REM XCOPY src\jquery.scrollTo\jquery.scrollTo.js                                        distr\scripts\system\ /Y
REM IF errorlevel 1 GOTO ERROR

ECHO ************* Copy Modernizr
XCOPY src\modernizr\modernizr.js                                                    distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

ECHO ************* Copy Moment
XCOPY src\moment\moment.js                                                          distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR

ECHO ************* Copy Moment
XCOPY src\bootstrap-datetimepicker\src\js\bootstrap-datetimepicker.js               distr\scripts\system\ /Y
IF errorlevel 1 GOTO ERROR
XCOPY src\bootstrap-datetimepicker\src\less\bootstrap-datetimepicker.less           distr\content\less\system\bootstrap-datetimepicker\ /Y
IF errorlevel 1 GOTO ERROR
XCOPY src\bootstrap-datetimepicker\build\css\bootstrap-datetimepicker.css           distr\content\ /Y
IF errorlevel 1 GOTO ERROR

COLOR 02
ECHO Copy successfull. Good luck.
EXIT /B

:ERROR
COLOR 04
ECHO Copy faild. See log for details.
