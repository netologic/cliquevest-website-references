@ECHO OFF
COLOR 07

ECHO vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

REM ECHO ************* Install grunt and bower
REM CALL npm install -g grunt-cli
REM IF errorlevel 1 GOTO ERROR
REM CALL npm install -g bower
REM IF errorlevel 1 GOTO ERROR

ECHO ************* Build jQuery
CD src\jquery
CALL npm install
IF errorlevel 1 GOTO ERROR
CALL grunt
IF errorlevel 1 GOTO ERROR
CD ..\..

ECHO ************* Build Bootstrap
CD src\bootstrap
CALL npm install
IF errorlevel 1 GOTO ERROR
CALL grunt dist --force
IF errorlevel 1 GOTO ERROR
CD ..\..

ECHO ************* Build jQuery validation
CD src\jquery-validation
CALL npm install
IF errorlevel 1 GOTO ERROR
CALL grunt concat
IF errorlevel 1 GOTO ERROR
CD ..\..

REM ECHO ************* Build and copy jQuery browser
REM CD src\jquery-browser-plugin
REM CALL npm install
REM IF errorlevel 1 GOTO ERROR
REM CALL grunt
REM IF errorlevel 1 GOTO ERROR
REM CD ..\..

REM ECHO ************* Build Modernizr
REM CD src\modernizr
REM CALL npm install
REM IF errorlevel 1 GOTO ERROR
REM CALL grunt build
REM IF errorlevel 1 GOTO ERROR
REM CD ..\..

COLOR 02
ECHO Build successfull. Good luck.
ECHO ---------------------------------------------------------------------
EXIT /B

:ERROR
COLOR 04
ECHO Build faild. See log for details.
ECHO ---------------------------------------------------------------------
